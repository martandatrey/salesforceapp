package com.cloudroute.exactech;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainWebView extends AppCompatActivity {
    WebView mainWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_web_view);
        init();
        mainWebView.setWebViewClient(new HelloWebViewClient());
        mainWebView.setWebChromeClient(new HelloWebChromeClient());
        mainWebView.canGoBackOrForward(1000);
        mainWebView.setInitialScale(1);
        mainWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mainWebView.setVerticalScrollBarEnabled(true);

        try {
            WebSettings settings = mainWebView.getSettings();
            settings.setJavaScriptCanOpenWindowsAutomatically(true);
            settings.setSupportMultipleWindows(true);
            settings.setBuiltInZoomControls(true);
            settings.setJavaScriptEnabled(true);
            settings.setAppCacheEnabled(true);
            settings.setDatabaseEnabled(true);
            settings.setDomStorageEnabled(true);
            settings.setGeolocationEnabled(true);
            settings.setSaveFormData(false);
            settings.setUseWideViewPort(true);
            settings.setLoadWithOverviewMode(true);
            settings.setSaveFormData(true);
            settings.setAllowFileAccess(true);
            settings.setAllowUniversalAccessFromFileURLs(true);
            settings.setDomStorageEnabled(true);
            // Geo location settings
        } catch (Exception e) {
            Log.d("test", "Error configuring web view " + e);
        }

        mainWebView.loadUrl("https://exactech.force.com/s/");
    }

    private void init() {
        mainWebView = findViewById(R.id.MainWebView);
    }

    @Override
    public void onBackPressed() {
        if (mainWebView.isFocused() && mainWebView.canGoBack()) {
            if (mainWebView.getUrl().equals("https://exactech.force.com/s/")) {
                finish();
            }
            mainWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    private class HelloWebChromeClient extends WebChromeClient {
        @Override
        public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result) {
            Log.d("alert", message);
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            result.confirm();
            return true;
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog,
                                      boolean isUserGesture, android.os.Message resultMsg) {

            WebView newWebView = new WebView(MainWebView.this);
            view.addView(newWebView);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(newWebView);
            resultMsg.sendToTarget();

            newWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                    browserIntent.setData(Uri.parse(view.getUrl()));
                    startActivity(browserIntent);
                    return true;
                }
            });
            return true;
        }
    }

    private class HelloWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            Toast.makeText(getApplicationContext(), "Oh no! " + error, Toast.LENGTH_SHORT).show();
        }
    }
}
